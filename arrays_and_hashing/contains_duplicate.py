"""
Leetcode 217

Given an integer array nums, return true if any value appears at
least twice in the array, and return false if every element is distinct.

Example 1:
Input: nums = [1,2,3,1]
Output: true

Example 2:
Input: nums = [1,2,3,4]
Output: false

Example 3:
Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true
"""

def containsDuplicate(self, nums):
    # Create an empty haskset
    nums_set = set()
    # Iterate through nums
    for num in nums:
        # Check if num exists in hashset
        if num in nums_set:
            # If num exists in hashset return True, num is a duplicate
            return True
        # If nums is not in hashset, add num to hashset
        nums_set.add(num)
    return False
