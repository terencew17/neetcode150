"""
Leetcode #49

Given an array of strings strs, group the anagrams together. You can
return the answer in any order. An Anagram is a word or phrase formed
by rearranging the letters of a different word or phrase, typically
using all the original letters exactly once.

Example 1:
Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Example 2:
Input: strs = [""]
Output: [[""]]

Example 3:
Input: strs = ["a"]
Output: [["a"]]

"""

def groupAnagrams(self, strs):
    # Create empty dictionary to store grouped anagrams
    group = {}
    # Iterate through each word in the array
    for word in strs:
        # Sort the words alphabetically, then join it back into a string ex: "eat" -> "aet"
        sorted_str = "".join(sorted(word))
        # Check if the word exists in the dictionary
        if sorted_str in group:
            # If the word exists, append it the word to the list associated with that key
            group[sorted_str].append(word)
        else:
            # Create a new key value pair if word does not exist in dictionary
            group[sorted_str] = [word]
    # Return the anagrams
    return group.values()
