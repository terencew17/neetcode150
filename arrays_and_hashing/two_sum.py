"""
Leetcode 1

Given an array of integers nums and an integer target, return indices of the
two numbers such that they add up to target. You may assume that each input
would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.

Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].

Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]

Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
"""

def twoSum(self, nums, target):
    # Create dict to store element and index
    num_dict = {}

    # Iterate through nums array using enumerate(keeps track of index)
    for i, num in enumerate(nums):
        # Calculate difference between target number and current number
        diff = target - num
        # If the difference exists in dict, it is the pair that adds to target
        if diff in num_dict:
            # Return the index of both numbers
            return [num_dict[diff], i]
        # If difference doesn't exist, add current number to the dict w/ index
        num_dict[num] = i
    # If no pair is found return None
    return None
