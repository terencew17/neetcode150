"""
Leetcode 242

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different
word or phrase, typically using all the original letters exactly once.

Example 1:
Input: s = "anagram", t = "nagaram"
Output: true

Example 2:
Input: s = "rat", t = "car"
Output: false
"""

def isAnagram(self, s, t):
    # Check if the length of string 1 is equal to string 2
    # If length is not the same, then it cannot be an anagram
    if len(s) != len(t):
        return False
    # Create dictionary for each string to count occurence of each letter
    count_s = {}
    count_t = {}

    for letter in s:
        # Count the occurrence of each character. Use get() method to get the current
        # count of each character in dictionary. If not in dictionary, add and set to 0
        # add 1 to the count and update
        count_s[letter] = count_s.get(letter, 0) + 1

    for letter in t:
        count_t[letter] = count_t.get(letter, 0) + 1
    # If both dictionaries are same, return True
    return count_s == count_t
