"""
Leetcode 206

Given the head of a singly linked list, reverse the list, and return the reversed list.

Example 1:
Input: head = [1,2,3,4,5]
Output: [5,4,3,2,1]

Example 2:
Input: head = [1,2]
Output: [2,1]

Example 3:
Input: head = []
Output: []
"""

class Solution(object):
    def reverseList(self, head):
        # Set a variable to None to keep track of the previous node
        prev = None
        # Initializes a variable to traverse the list
        current = head
        # Start a while loop
        while (current is not None):
            # Keep track of the next node of the current node
            next_node = current.next
            # Reverse the link of the current node to point to the previous node
            current.next = prev
            # Update the previous variable to point to the current node
            prev = current
            # Move the pointer to the next node to keep traversing the list
            current = next_node
        # Return the reversed list
        return prev
