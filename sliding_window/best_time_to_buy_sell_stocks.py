"""
Leetcode #121

You are given an array prices where prices[i] is the price of a given stock
on the ith day. You want to maximize your profit by choosing a single day to
buy one stock and choosing a different day in the future to sell that stock.
Return the maximum profit you can achieve from this transaction. If you
cannot achieve any profit, return 0.


Example 1:
Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

Example 2:
Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transactions are done and the max profit = 0.

"""

def maxProfit(self, prices):
    # Create variable to keep track of max profit and min price
    max_profit = 0
    # Set min price to the first element in the array
    min_price = prices[0]
    # Iterate through the array starting at the second element
    for i in range(1, len(prices)):
        # Calculate the potential profit by subtracting the current price from the minimum price encountered so far
        profit = prices[i] - min_price
        # Update the maximum profit if the potential profit is greater
        if profit > max_profit:
            max_profit = profit
        # Update the minimum price if the current price is smaller
        if prices[i] < min_price:
                min_price = prices[i]
    # Return max profit
    return max_profit
