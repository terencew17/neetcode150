"""
Leetcode #20

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']',
determine if the input string is valid.

An input string is valid if:
Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.

Example 1:
Input: s = "()"
Output: true

Example 2:
Input: s = "()[]{}"
Output: true

Example 3:
Input: s = "(]"
Output: false

"""

class Solution(object):
    def isValid(self, s):
        # If length of string is not even return False
        if len(s) % 2 == 1:
            return False
        # Initialize an empty stack to keep track of open brackets.
        stack = []
        # Iterate through each character in the input string.
        for i in s:
            # If the current character is an open bracket ('(', '{', '['), push it onto the stack.
            if i == "(" or i == "[" or i == "{":
                stack.append(i)
            # If the current character is a close bracket (')', '}', ']'), check if the stack is empty.
            elif i == ")" or i == "]" or i == "}":
                if len(stack) == 0:
                    # If it is, return False because there is no corresponding open bracket.
                    return False
                # Pop the top element from the stack, representing the most recently opened bracket.
                first = stack.pop()
                # Check if the current closing bracket i matches the corresponding opening bracket
                if (i == ')' and first != '(') or (i == '}' and first != '{') or (i == ']' and first != '['):
                    return False
        # After pairing, if stack is not 0, there are unpaired brackets, return False
        return len(stack) == 0
