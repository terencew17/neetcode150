"""
Leetcode #125

A phrase is a palindrome if, after converting all uppercase letters into lowercase
letters and removing all non-alphanumeric characters, it reads the same forward
and backward. Alphanumeric characters include letters and numbers.
Given a string s, return true if it is a palindrome, or false otherwise.


Example 1:
Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Example 2:
Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.

Example 3:
Input: s = " "
Output: true
Explanation: s is an empty string "" after removing non-alphanumeric characters.
Since an empty string reads the same forward and backward, it is a palindrome.
"""

def isPalindrome(self, s):
    # Create variable to store new string with only alphanumeric char
    new_string = ""
    # Iterate through the string
    for char in s:
        # Check if character is alphanumeric using isalnum()
        if char.isalnum():
            # Add char to variable and lowercase
            new_string += char.lower()
    # Return true if string is equal to string reversed
    return new_string == new_string[::-1]
